#!/bin/sh -e

VERSION=$2
TAR=../colapi_$VERSION.orig.tar.gz
DIR=colapi-$VERSION
TAG=$(echo "colapi-$VERSION" | sed -re's/~(alpha|beta)/-\1-/')

svn export http://svn.java.net/svn/javolution~svn/tags/${TAG}/ $DIR
GZIP=--best tar -c -z -f $TAR --exclude '*.jar' --exclude '*.class' $DIR
rm -rf $DIR ../$TAG

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
  . .svn/deb-layout
  mv $TAR $origDir && echo "moved $TAR to $origDir"
fi
